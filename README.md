# Usha_Company_Master

Company Master (Dataset)

1.Go inside the Public folder, inside this folder it contains
    a.csvFile folder.
    b.JSFiles folder.
    c.index.html.
    d.JSON Files.

2.In csvFile folder, it contain company_data.csv file, which is required to solve the problem.

3.JSFile folder, it contains all javascript files and index.js(solution for the problem) is the main javascript file,
  you can run this file on 'node platform'(./public/JSFile/index.js => node index.js).

4.Then, run index.html file (./public/index.html => serve .).

5.In the browser search http address with port number (eg. http://localhost:5000)  



