fetch("years_graph2.json")
  .then((resp) => resp.json())
  .then(function (data) {
    const key = Object.keys(data);
    const vs = Object.values(data);
    Highcharts.chart('container2', {
      chart:{
        type:'column'
      },
      title:{
        text:'NO_OF_COMPANY_REGISTERED_BY_YEAR'
      },
      xAxis:{
        categories:key,
        text:'YEAR'
      },
      yAxis:{
        min:0,
        title:{
          text:'NO_OF_COMPANY_REGISTERED'
        }
      },
      legend: {
        reversed:true,
      },
      plotOptions:{
        series:{
          stacking:'normal',
        },
      },
      series:[{
        name:'COMPANY_REGISTERED_YEAR',
        data:vs,
        colorByPoint:true,
        dataLabels:{
          enabled:true,
          rotation:-45,
          color:'#FFFFFF',
          align:'right',
          format:'{point.y:.1f}',
          y:10,
          style:{
            fontSize:'13px',
            fontFamily:'Verdana, sans-serif',
          }
        }
      }],
    });
  });
